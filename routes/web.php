<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TodoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [TodoController::class, 'index']);

Route::get('create', [TodoController::class, 'create']);
Route::post('create', [TodoController::class, 'store']);

Route::get('details/{todo}', [TodoController::class, 'details']);

Route::get('edit/{todo}', [TodoController::class, 'edit'])->name('edit');
Route::post('update/{todo}', [TodoController::class, 'update'])->name('update');
Route::get('delete/{todo}', [TodoController::class, 'delete']);

//Route::get('delete/{todo}', [TodoController::class, 'delete'])->name('delete');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
